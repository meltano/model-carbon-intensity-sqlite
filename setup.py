from setuptools import setup, find_packages

setup(
    name='model carbon intensity sqlite',
    version='0.1',
    description='Models for carbon intensity data sqlite edition',
    packages=find_packages(),
    package_data={'models': ['*.m5o']},
    install_requires=[],
)